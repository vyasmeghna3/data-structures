// Hash Table.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>
using namespace std;

// Structure defining the entry node structure
struct Entry
{
	// Field to hold the entry's key
	int key;

	// Field to hold the entry's value
	string value;

	// Field to hold pointer to the next entry
	Entry* next;

	// Constructor
	Entry(int k, string v)
	{
		key = k;
		value = v;
		next = nullptr;
	}
};

// Class defining the hash table
class HashTable
{
	// Field to hold pointer to pointer to the table
	Entry** table;

	// Field to hold the size of the table
	int size;

public:

	// Constructor which sets default value of table size as 10
	HashTable(int s = 10)
	{
		size = s;

		// [size] means allocate array of size
		table = new Entry * [size];

		// As we know, everything in the heap has garbage value unless initialized
		// Using a loop to traverse through the table and initialize it to null
		for (int i = 0; i < size; i++)
			table[i] = nullptr;
	}

	// Method that takes a key and returns an offset
	// Hash function adds the hashing functionality, making it a hash table
	int hashFunction(int k)
	{
		return k % size;
	}

	// Method to put or make a new entry in the hash table
	bool put(int k, string val)
	{
		// Get the offset by hashing the given key
		int offset = hashFunction(k);

		// Make a new entry
		Entry* newEntry = new Entry(k, val);

		// Check if new entry is allocated
		if (!newEntry)
			return false;

		// So, the new node is allocated 
		// If there is already an entry with the same offset
		if (table[offset])
		{
			// Insert the new entry before the first entry in the same offset
			newEntry->next = table[offset];
		}
		table[offset] = newEntry;
	}

	// Function to that takes a key from user and gives the corresponding value
	bool get(int k)
	{
		// Get offset of key using hash function
		int offset = hashFunction(k);

		// If not in the list
		if (!table[offset])
			return "Sorry requested key not found!";

		// Traverse the offset entries
		for (Entry* current = table[offset]; current; current = current->next)
		{
			{
				if (current->key == k)
					return current->value;
			}
		}
	}

	// Function to remove the entry with a given key
	bool remove(int k)
	{
		// Get the offset of key using hash function
		int offset = hashFunction(k);

		// If no entry with the calculated offset exists
		if (!table[offset])
			return false;

		// Traverse the offset entires 
		for (Entry* current = table[offset]; current; current = current->next)
		{
			// Pointer to keep track of the entry before current
			Entry* previous = current;

			// If we find the key
			if (current->key == k)
			{
				// Found at the first entry
				if (current == table[offset])
				{
					// Make the table point to the next entry
					// And delete the first node
					table[offset] = current->next;
					delete current;
					return true;
				}

				// Connect the entry before current entry to the one after the current entry 
				previous->next = current->next;

				// Delete the current node
				delete current;
				return true;
			}
		}

	}

	// Method to print the Hash Table
	void print()
	{
		cout << "\n\t\t HASHTABLE\n";

		// traverse the hashtable
		for (int i = 0; i < size; i++)
		{
			cout << " " << i + 1 << " : ";

			//Another loop to traverse the entries for each offset of table
			for (Entry* current = table[i]; current; current = current->next)
			{
				cout << "[" << current->key << ", " << current->value << "]";
			}

			cout << endl;
		}
	}
};

// Main method to control the flow of program
int main()
{
	// Field to hold entry's key entered by user
	int key;

	// Field to hold entry's value entered by user
	string value;

	// Field to check success status of any operation
	bool status = true;

	// Class HashTable instance
	HashTable table;

	// loop to get entry fields from user, and put a new entry into the hash table

	while (cout << "Enter the key, value to put (0 to stop) : ",
		   cin >> key >> value,
		   key)
	{
		status = table.put(key, value);
		if (!status)
			cout << "Put operation failed !" << endl;
	}



	/*
	// Get a value from the Hash Table, given the key
	cout << "Enter a key to get value : ";
	cin >> key;
	string getString = table.get(key);
	cout << getString;

	// Remove an entry from the Hash Table, given the key
	cout << endl << "Enter a key to remove the entry : ";
	cin >> key;
	status = table.remove(key);
	if (!status)
		cout << "Sorry requested key not found !" << endl;

	table.print();

	*/
	return 0; 
}
