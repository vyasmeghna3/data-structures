// Priority Queue.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>
using namespace std;

// Structure defining a node in the priority queue
struct Node
{
	// Field to hold the value
	string value;

	// Field to hold the priority of a node in the queue
	// This determines where the node goes in the queue
	int priority;

	// Field to hold pointer to the next node
	Node* next;

	// Parameterized constructor
	Node(string v, int p)
	{
		value = v;
		priority = p;
		next = nullptr;
	}
};

// Class to define the priority queue
class PriorityQueue
{
	// Field to hold pointer to the first node
	Node* head;

	// Field to hold pointer to the last node
	Node* tail;

public:
	// Default constructor
	PriorityQueue()
	{
		head = nullptr;
		tail = nullptr;
	}

	// Destructor
	~PriorityQueue()
	{
		// Called to free the memory before we exit the code
		removeAll();
	}

	/*****************************************************************
	*			Operations
	* push() - pushes a node into the queue according to its priority
	* remove() - removes the first node from the queue
	* removeAll() - removes all the nodes from the queue
	* print() - prints the nodes of the queue from head to tail
	*****************************************************************/

	// Function to push a node to the queue
	bool push(string val, int pri)
	{
		// Create a new node
		Node* newNode = new Node(val, pri);

		// Check if allocated or not
		if (!newNode)
			return false;

		// Check if this would be the first node
		if (!head)
		{
			// The head and tail would point to the same node; the first node now
			head = tail = newNode;
			return true;
		}

		// So the queue already has a few nodes
		// Traverse the queue and find the apt place for the node to be inserted
		for (Node* current = head; current; current = current->next)
		{
			// A node pointer to keep track to the node before current node
			Node* previous = head;

			// Check if the new node's priority is lesser than the current node
			// Then push it before the current node
			// As 1(max priority)
			if (current->priority > pri)
			{
				// This might be the first node
				// New node's priority is lesser than that of the first node
				if (current == head)
				{
					newNode->next = head;
					head = newNode;
					return true;
				}

				// The correct node might be somewhere after the first node
				// So create a pointer; previous, to keep track of the node before current node
				// (declared at beginning)
				// This pointer initially points to head
				// And at the end of the loop it points to current
				// But just right then, for the next iteration current moves forward (current = current->next)
				// So this pointer always points to the node just before current node
				previous->next = newNode;
				return true;
				
			}
			previous = current;
		}

		tail->next = newNode;
		tail = newNode;
		return true;
	}

	// Method to remove a node from the queue
	void remove()
	{
		// Check if the queue is already empty
		if (!head)
			cout << "Queue is empty";

		// The queue is not empty
		else
		{
			// Create a new node pointer, temp to point to the head node
			Node* temp = head;

			// Move the head to its next node
			head = head->next;

			// Now delete temp, the node which was previously the head node
			delete temp;
		}
	}

	// Method to remove all the nodes, head to tail from the queue
	void removeAll()
	{
		for (Node* current = head; current; current = current->next)

			delete current;
	}

	// Method to print all the nodes, head to tail in the queue
	void print()
	{
		// Check if the queue has no node
		// the head points to null in this condition
		if (!head)
			cout << "Queue is empty : ";

		// The queue is not empty
		else

			// Start traversing from head and print every node
			for (Node* current = head; current; current = current->next)
				cout << current->value << ", " << current->priority << endl;
	}
};


// Main method to control the program flow
int main()
{
	// Field to hold the node value entered by user
	string userValue;

	// Field to hold the node priority entered by user
	int userPriority;

	// object of class PriorityQueue created
	// This object represents the airport queue for flight boarding
	// We see 1 has the highest priority for the flight cabin crew
	// 2 is the second highest priority for the wheelchair bound passengers
	// 3 is the priority number for the common passengers, who find themselves as the last priority in the queue
	PriorityQueue flightBoarding;

	flightBoarding.push("Meghna", 3);
	flightBoarding.push("Neeraja", 1);
	flightBoarding.push("Geeta", 3);

	flightBoarding.print();
	
	flightBoarding.remove();

	flightBoarding.print();

	flightBoarding.removeAll();
	flightBoarding.print();
	return 0;
}

