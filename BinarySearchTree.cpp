// Binary Search Tree (OOP).cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>

using namespace std;

// Structure defining a tree node
struct Node
{
	// Field to hold pointer to left node
	Node *left;

	// Field to hold value of node
	int value;

	// Field to hold pointer to right node
	Node *right;

	// Constructor
	Node(int v)
	{
		left = nullptr;
		value = v;
		right = nullptr;
	}
};

// Class to define the Binary Search Tree
class BST
{
	// Field to hold pointer to the root node of the tree
	Node *root;

	// Method to add a node to the tree
	Node *addNode(Node *node, int val)
	{
		// Firstly, the stopping condition
		// If we've reached the end, or the tree has no node
		// this is where you add the new node
		if (!node)
			return new Node(val);

		// Now check if the value of new node is lesser than that of root node
		// Is less, get into the left subtree
		if (val < node->value)
			node->left = addNode(node->left, val);

		// If new node's value is greater then the root node
		// Get into the right subtree
		else
			node->right = addNode(node->right, val);
	}

	// Method to print the Binary Search Tree inorder
	// takes root node as argument
	Node *printInorder(Node *node)
	{
		// Check if tree has no node
		if (!node)
			return nullptr;

		// Tree has nodes
		// So first process the left subtree
		printInorder(node->left);

		// Print the node
		cout << node->value;

		// Process the right subtree
		printInorder(node->right);
	}

	/******************************************************************
	* 	ABSTRACTOR METHODS
	* These are the only methods that can be accessed outside the class
	******************************************************************/

public:
	// Constructor
	BST()
	{
		root = NULL;
	}

	// Method to call addNode() passing root as argument
	Node *add(int val)
	{
		root = addNode(root, val);
	}

	Node *print()
	{
		printInorder(root);
	}
};

int main()
{
	BST tree;
	tree.add(10);
	tree.add(5);
	tree.add(2);
	tree.print();
	return 0;
}
