// Sentinel List.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

//structure to define a node
struct Node
{
	//Fields
	Node* prev;
	int value;
	Node* next;
	//Constructor
	Node(int v)
	{
		prev = nullptr;
		value = v;
		next = nullptr;
	}
};

//class contains the head and tail pointers, and operations to add, remove and print the list
class Sentinel
{
	//Fields
	//a dummy head node
	Node* head;
	//a dummy tail node
	Node* tail;
public:
	//Constructor
	Sentinel()
	{
		head = new Node(0);
		tail = new Node(0);
		head->next = tail;
		tail->prev = head;
	}

	/********************************************************************
	*		Operations
	* addToFont() - to add a value at the front the list
	* addToLast() - to add a value to the last of the list
	* remove() - to remove a value from before or after a value from the list
	* print() - to display all the values in the list
	*/
	bool addToFront(int val)
	{
		//allocate a new node
		Node* newNode = new Node(val);
		//connect the new node to the front of the list
		head->next->prev = newNode;
		head->next->prev->next = head->next;
		head->next = head->next->prev;
		head->next->prev = head;
		return true;
	}

	bool remove(int val)
	{
		//set up the traversal to find the node to be removed
		for (Node* current = head->next; current != tail; current = current->next)
		{
			//if node found
			if (current->value == val)
			{
				current->next->prev = current->prev;
				current->prev->next = current->next;
				return true;
			}
			else
				return false;
		}
	}

	void print()
	{
		cout << "Elements of the list are : ";
		for (Node* current = head->next; current != tail; current = current->next)
		{
			cout << current->value << "  ";
		}
	}
};

int main()
{
	Sentinel list;
	list.addToFront(5);
	list.addToFront(3);
	list.remove(5);
	list.print();
	return 0;
}