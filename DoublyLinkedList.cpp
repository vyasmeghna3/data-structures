// Linked List.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;
//structure defining the nodes
struct Node {
	//fields
	int value;
	struct Node* next;
	struct Node* prev;
	//constructor
	Node(int v)
	{
		value = v;
		next = nullptr;
		prev = nullptr;
	}
};

class List {
	//fields
	Node* head;
	Node* tail;
public:
	//constructor
	List() {
		head = nullptr;
		tail = nullptr;
	}
	/*****************************************************************
	* Operations*
	*insertAtFront() - insert a value a front
	*insertAtLast() - insert a value at the last
	*insertBefore() - insert a value before a value in the list
	*insertAfter() - insert a value after a value in the list
	******************************************************************
	*/
	bool insertAtFront(int valueToInsert)
	{
		//create a new node
		Node* newNode = new Node(valueToInsert);
		//a current pointer to point to the head
		Node* current = head;
		//if no node?
		if (nullptr == head)
		{
			tail = newNode;
		}
		else
		{
			newNode->next = head;
			head->prev = newNode;
		}
		head = newNode;
		return true;
	}

	bool insertAtLast(int val)
	{
		Node* newNode = new Node(val);
		Node* current = tail;
		if (nullptr == head)
		{
			head = newNode;
		}
		else
		{
			newNode->prev = tail;
			tail->next = newNode;
		}
		tail = newNode;
		return true;
	}

	bool insertBefore(int valueToSearch, int valueToInsert)
	{
		Node* newNode = new Node(valueToInsert);
		for (Node* current = head; current; current = current->next)
		{
			if (current->value == valueToSearch)
			{
				if (current == head)		//first element
				{
					insertAtFront(valueToInsert);
					return true;
				}
				else
				{
					newNode->next = current;
					newNode->prev = current->prev;
					current->next->prev = newNode;
					current->prev = newNode;
					return true;
				}
			}
			else
				return false;

		}

	}

	bool insertAfter(int val, int num)
	{
		Node* newNode = new Node(val);
		for (Node* current = head; current; current = current->next)
		{
			if (current->value == num)
			{
				if (current == tail)		//last element
				{
					insertAtLast(val);
					return true;
				}
				else
				{
					newNode->prev = current;
					newNode->next = current->next;
					current->next = newNode;
					current->next->prev = newNode;
					return true;
				}
			}
		}
	}
	/*
	*Operation that removes the given value from the list
	*/
	bool remove(int val)
	{
		//set up traversal to find the given value
		for (Node* current = head; current; current = current->next)
		{
			//if you find the value in the list
			if (current->value == val)
			{
				//only one node in the list
				if (head == tail)
				{
					head = tail = nullptr;
					delete current;
					return true;
			}
				//the given value found at first position
				if (current == head)
				{
					head = head->next;
					delete current;
					head->prev = nullptr;
					return true;
				}
				//the given node found at the last
				if (current == tail)
				{
					tail = tail->prev;
					delete current;
					tail->next = nullptr;
					return true;
				}
				//value found at element somewhere in the middle
				else
				{
					current->prev->next = current->next;
					current->next->prev = current->prev;
					return true;
				}
			}
		}
	}
	/*
	*Operation to print the elements of the list
	*/
	void print()
	{
		cout << "Elements of list are : ";
		for (Node* current = head; current; current = current->next)
			cout << current->value << "  ";

	}
};

int main()
{
	int value=0, searchValue=0;
	List shoppingList, teachingList;
	while (cout << "Enter a value to insert at front (0 to stop)", cin >> value, value)
	{
		shoppingList.insertAtFront(value);
		shoppingList.print();
	}
	shoppingList.print();
	return 0;
}


