#include <iostream>
using namespace std;

struct Node
{
	int value;
	Node* next;
	Node(int v)
	{
		value = v;
		next = nullptr;
	}
};

class Queue
{
	Node* front;
	Node* rear;

public:
	Queue()
	{
		front = nullptr;
		rear = nullptr;
	}

	bool push(int val)
	{
		// Create a new node
		Node* newNode = new Node(val);

		if (!newNode)
			return false;

		// Check if this is the first node
		if (front == rear)
		{
			front = newNode;
			rear = front;
			return true;
		}

		// Not the first node
		rear->next = newNode;
		rear = rear->next;
	}

	bool pop()
	{
		// Check if the list is empty
		if (!rear && !front)
			return false;

		// List is not empty
		Node* temp = front;
		front = front->next;
		delete temp;
		return true;
	}

	void print()
	{
		if (!front)
			cout << "Queue is empty";
		else
			for (Node* current = front; current; current = current->next)
				cout <<endl<<current->value << "  ";

	}

};

int main()
{
	int userValue;
	Queue queue1;
	while (cout << "Enter a value to push (0 to stop) : ",
		cin >> userValue,
		userValue)
		queue1.push(userValue);
	airportBoarding.print();

	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
