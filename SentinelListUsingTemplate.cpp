// Sentinel List using template.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

// Template for structure of a node
template <class T>
struct Node
{
	// Field to hold the pointer to previous node
	Node* prev;

	// Field to hold value of node
	T value;

	// Field to hold the pointer to next node
	Node* next;

	// Parameterized constructor
	Node(T val)
	{
		prev = nullptr;
		value = val;
		next = nullptr;
	}
};

// template for class defining the Sentinel list
template <class T>
class Sentinel
{
	// Field to hold pointer to the first dummy node
	Node<T>* head;

	// Field to hold pointer to the last dummy node
	Node<T>* tail;

public:
	// Constructor
	Sentinel()
	{
		// dummy nodes for the sentinel list having value 0
		head = new Node<T>(0);
		tail = new Node<T>(0);

		// Connect the two dummy nodes
		head->next = tail;
		tail->prev = head;
	}

	// Function that inserts a node to the list
	// it takes a node after which the new node is to be inserted 
	// and value of the new node from the calling function
	bool insert(Node<T> *node, int val)
	{
		// Create a new node with the passed value
		Node<T>* newNode = new Node<T>(val);

		// Check if new node not allocated
		if (!newNode)
			return false;

		// Connect this node after the node passed as argument to the function
		newNode->next = node->next;
		newNode->prev = node;
		node->next->prev = newNode;
		node->next = newNode;
		return true;
	}

	// Method to insert a new node after a node with a given value
	bool insertAfter(int valueToSearch, int valueToInsert)
	{
		// Traverse the list to find the existing node with given value
		for (Node<T>* current = head->next; current; current = current->next)
		{
			if (current->value == valueToSearch)
			{
				return insert(current, valueToInsert);
			}
		}
	}

	// Method to insert a node before a node with a given value
	bool insertBefore(int valueToSearch, int &valueToInsert)
	{
		for (Node<T>* current = head->next; current; current = current->next)
		{
			if (current->value == valueToSearch)
				return insert(current->prev);
		}
	}

	// Method to insert a node after the head node
	bool insertAtFront(int val)
	{
		return insert(head, val);
	}

	// Method to insert a node before the tail node
	bool insertAtLast(int val)
	{
		return insert(tail->prev, val);
	}

	// Method to print the Sentinel list
	bool print()
	{
		// Start traversing from head->next, which is indeed the first node
		// and stop when you reach the tail
		for (Node<T>* current = head->next; current != tail; current = current->next)
		{
			if (current->next == tail)
				cout << current->value << endl;
			else
			cout << current->value << " -> ";
		}
		return true;
	}

	// Method that takes a given value and removes it from the list
	bool remove(int val)
	{
		// Traverse the list
		for (Node<T>* current = head->next; current != tail; current = current->next)
		{
			// We find the value in the list
			if (current->value == val)
			{
				// Connect the node before the current node to the node after it
				current->next->prev = current->prev;
				current->prev->next = current->next;
				return true;
			}
		}

		// When we end up not finding the value in the list
		return false;
	}

	// Method to replace a particular occurance of the given value from the list
	// If occurance is not mentioned, it by default replaces the first occurance of the value
	bool replaceOccurance(int valueToReplace, int newValue, int occurance = 1)
	{
		// To keep a count on the occurances of a value
		int count = 0;

		// loop to find value in the list
		for (Node<T>* current = head->next; current != tail; current = current->next)
		{
			// If we find the value to replace
			if (current->value == valueToReplace)
			{
				// Increase the count of the value
				count++;

				// If the occurance value matches the count value
				// Replace the current node's value with the new value
				if (occurance == count)
				{
					current->value = newValue;
					return true;
				}
			}
		}

		// When we end up not finding the value in the list
		return false;
	}
};

// Main method, the control begins here
int main()
{
	// Field to hold valueToSearch entered by user
	int userValue;

	// Field to hold the new value to be inserted into the list 
	int newValue;

	// Field to hold the number of occurance given by the user
	int occurance;

	// Field to hold the status of success of any operation
	bool status = true;

	// Sentinel class instance
	Sentinel<int> list;

	// Loop to take value from user to insert at last
	while (cout << "Enter a value to insert at last (0 to stop) : ",
	       cin >> userValue,
		   userValue)
	{
		status = list.insertAtLast(userValue);
		if (!status)
			cout << "Insert operation failed !" << endl;
	}
	list.print();

	// Loop to take value from user and remove it from the list
	while (cout << "Enter a value to remove (0 to stop) : ",
		   cin >> userValue,
		   userValue)
	{
		status = list.remove(userValue);
		if (!status)
			cout << "Sorry requested value not found"<<endl;
	}

	list.print();

	// Replace a particular occurance of a value with a new value given by user
	cout << "Enter a value to replace, new value, occurance : ";
	cin >> userValue >> newValue>> occurance;
	status = list.replaceOccurance(userValue, newValue, occurance);
	if (!status)
		cout << "Sorry the requested value, " << userValue << " not found.";
	list.print();
	return 0;
}

